import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Comment } from '../../models/comment/comment';
import { empty, Observable, Subject } from 'rxjs';
import { User } from '../../models/user';
import { AuthenticationService } from '../../services/auth.service';
import { AuthDialogService } from '../../services/auth-dialog.service';
import { catchError, switchMap, takeUntil } from 'rxjs/operators';
import { LikeService } from '../../services/like.service';
import { CommentService } from '../../services/comment.service';
import { DialogType } from '../../models/common/auth-dialog-type'

@Component({
    selector: 'app-comment',
    templateUrl: './comment.component.html',
    styleUrls: ['./comment.component.sass']
})
export class CommentComponent {
    @Input() public comment: Comment;
    @Input() public currentUser: User;
    @Input() public canDelete: boolean;
    @Output() public deleted = new EventEmitter();

    public isEditing = false;
    private unsubscribe$ = new Subject<void>();

    public constructor(
        private authService: AuthenticationService,
        private authDialogService: AuthDialogService,
        private likeService: LikeService,
        private commentService: CommentService
    ) { }

    public likeComment() {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(
                    switchMap((userResp) => this.likeService.likeComment(this.comment, userResp)),
                    takeUntil(this.unsubscribe$)
                )
                .subscribe((comment) => (this.comment = comment));

            return;
        }

        this.likeService
            .likeComment(this.comment, this.currentUser)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((comment) => (this.comment = comment));
    }

    deleteComment() {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(
                    switchMap((userResp) => this.commentService.deleteComment(this.comment)),
                    takeUntil(this.unsubscribe$)
                )
                .subscribe((comment) => (this.comment = comment.body as Comment));

            return;
        }

        this.commentService
            .deleteComment(this.comment)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((comment) => {
                this.deleted.emit((comment.body as Comment).id)
            });
    }

    public updateComment() {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(
                    switchMap((userResp) => this.commentService.updateComment(this.comment)),
                    takeUntil(this.unsubscribe$)
                )
                .subscribe((comment) => (this.comment = comment.body as Comment));

            return;
        }

        this.commentService
            .updateComment(this.comment)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe();
        this.switchToEdit();
    };

    public openAuthDialog() {
        this.authDialogService.openAuthDialog(DialogType.SignIn);
    }

    private catchErrorWrapper(obs: Observable<User>) {
        return obs.pipe(
            catchError(() => {
                this.openAuthDialog();
                return empty();
            })
        );
    }

    public checkAuthor() {

        if (!this.currentUser)
            return false;

        return this.comment.author.id === this.currentUser.id;
    }

    public switchToEdit(): void {
        this.isEditing = !this.isEditing;
    }
}
