﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using Thread_.NET.BLL.Exceptions;
using Thread_.NET.BLL.Services.Abstract;
using Thread_.NET.Common.DTO.Comment;
using Thread_.NET.DAL.Context;
using Thread_.NET.DAL.Entities;

namespace Thread_.NET.BLL.Services
{
    public sealed class CommentService : BaseService
    {
        public CommentService(ThreadContext context, IMapper mapper) : base(context, mapper) { }

        public async Task<CommentDTO> CreateComment(NewCommentDTO newComment)
        {
            var commentEntity = _mapper.Map<Comment>(newComment);

            _context.Comments.Add(commentEntity);
            await _context.SaveChangesAsync();

            var createdComment = await _context.Comments
                .Include(comment => comment.Author)
                    .ThenInclude(user => user.Avatar)
                .FirstAsync(comment => comment.Id == commentEntity.Id);

            return _mapper.Map<CommentDTO>(createdComment);
        }

        public async Task<CommentDTO> DeleteComment(CommentDTO commentDTO) 
        {            
            var commentReactions = _context.CommentReactions.Where(cr => cr.Comment.Id == commentDTO.Id);
            var commentEntity = _context.Comments.Where(c => c.Id == commentDTO.Id).First();

            _context.CommentReactions.RemoveRange(commentReactions);
            _context.Comments.Remove(commentEntity);

            await _context.SaveChangesAsync();

            return commentDTO;
        }

        public async Task UpdateComment(CommentDTO commentDto)
        {
            var commentEntity = GetCommentByIdInternal(commentDto.Id);
            if (commentEntity == null)
            {
                throw new NotFoundException(nameof(Comment), commentDto.Id);
            }

            var timeNow = System.DateTime.Now;

            commentEntity.Body = commentDto.Body;
            commentEntity.UpdatedAt = timeNow;

            _context.Comments.Update(commentEntity);
            await _context.SaveChangesAsync();
        }

        private Comment GetCommentByIdInternal(int id)
        {
            return _context.Comments.Where(c => c.Id == id).First();
        }

    }
}
