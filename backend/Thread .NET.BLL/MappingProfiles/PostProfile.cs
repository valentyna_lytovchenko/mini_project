﻿using AutoMapper;
using Thread_.NET.Common.DTO.Post;
using Thread_.NET.DAL.Entities;
using System.Collections;
using System.Linq;
using Thread_.NET.DAL.Entities.Abstract;

namespace Thread_.NET.BLL.MappingProfiles
{
    public sealed class PostProfile : Profile
    {
        public PostProfile()
        {
            CreateMap<Post, PostDTO>()
                .ForMember(dest => dest.PreviewImage, src => src.MapFrom(x => x.Preview != null ? x.Preview.URL : string.Empty));

            CreateMap<PostCreateDTO, Post>()
                .ForMember(dest => dest.Preview, src => src.MapFrom(s => string.IsNullOrEmpty(s.PreviewImage) ? null : new Image { URL = s.PreviewImage }));

            CreateMap<PostDTO, Post>()
                .ForMember(dest => dest.Preview, src => src.MapFrom(s => string.IsNullOrEmpty(s.PreviewImage) ? null : new Image { URL = s.PreviewImage }))
                .ForMember(dest => dest.Author, src => src.MapFrom(s => s.Author))
                .ForMember(dest => dest.Reactions, src => src.MapFrom(s => s.Reactions));
        }
    }
}
